package io.fintechlabs.testframework.testmodule;

/**
 * Marker interface to allow collections of ConditionCallBuilder and TextExecutionBuilder instances.
 */
public interface TestExecutionUnit {

}
